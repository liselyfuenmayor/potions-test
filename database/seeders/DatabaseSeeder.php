<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Models\User; 
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create();

        $seeders = [
            ClientSeeder::class,
            IngredientSeeder::class,
            PotionSeeder::class,
            SaleSeeder::class
        ];

        foreach($seeders as $class) {
            $this->call($class);
        }

    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Repositories\Interfaces\IngredientRepositoryInterface;

class IngredientSeeder extends Seeder
{
    const INGREDIENTS = [
        [
            'name'    => 'Aloe vera',
            'description'    => 'Plant',
            'price'    => 1500,
            'identifier' => 'aloe'
        ],
        [
            'name'    => 'Cenizas',
            'description'    => 'Witch Ashes',
            'price'    => 2500,
            'identifier' => 'ashes'
        ],
        [
            'name'    => 'Jugo Magico',
            'description'    => 'it is a mistery',
            'price'    => 27000,
            'identifier' => 'jugo'
        ],
        [
            'name'    => 'Lagrimas de gato',
            'description'    => 'sad kitty',
            'price'    => 9000,
            'identifier' => 'kitty'
        ],
        [
            'name'    => 'Petalos',
            'description'    => 'sandals',
            'price'    => 2000,
            'identifier' => 'sandals'
        ],
        [
            'name'    => 'Polvo de cuerno de bicornio',
            'description'    => 'its really ?',
            'price'    => 65000,
            'identifier' => 'bicornio'
        ],
        [
            'name'    => 'Polvo magico',
            'description'    => 'magic',
            'price'    => 30000,
            'identifier' => 'magic'
        ],
        [
            'name'    => 'Sal de mar',
            'description'    => 'common',
            'price'    => 3000,
            'identifier' => 'salt'
        ],
        [
            'name'    => 'Sanguijuelas',
            'description'    => 'disgusting',
            'price'    => 13000,
            'identifier' => 'leech'
        ], 
        [
            'name'    => 'Vino',
            'description'    => 'delicious',
            'price'    => 6000,
            'identifier' => 'wine'
        ], 
    ]; 
    /**
     * @var object
     */
    protected $ingredientRepository;

    /**
     * Instances a new seeder class.
     */
    public function __construct(
        IngredientRepositoryInterface $ingredientRepository
    ) 
    {
        $this->ingredientRepository = $ingredientRepository;
    }

    /**
     * stored clients in the database.
     *
     * @return \Iterator
     */
    private function registerIngredients($ingredients)
    {
        foreach ($ingredients as $ingredient) {
                $data = [
                    'name' =>  $ingredient['name'],
                    'description' => $ingredient['description'],
                    'price' => $ingredient['price'],
                    'identifier' => $ingredient['identifier']
                ];

                $ingredient_create = $this->ingredientRepository->create($data);
 
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->registerIngredients(self::INGREDIENTS);
    }
}

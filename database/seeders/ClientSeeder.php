<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Repositories\Interfaces\ClientRepositoryInterface;

class ClientSeeder extends Seeder
{
    const CLIENTS = [
        [
            'name'    => 'Elly',
            'last_name'    => 'Kedward',
            'profession'    => 'Master Witch',
            'identifier'          => 'A120X32'
        ],
        [
            'name'    => 'Alice',
            'last_name'    => 'Kyteler',
            'profession'    => 'Beginner Witch',
            'identifier'          => 'A120X33'
        ],
        [
            'name'    => 'Joan',
            'last_name'    => 'Wytte',
            'profession'    => 'Beginner Witch',
            'identifier'          => 'A120X34'
        ],
        [
            'name'    => 'Madame',
            'last_name'    => 'Blavatsky',
            'profession'    => 'Master Witch',
            'identifier'          => 'A120X35'
        ],
    ]; 
    /**
     * @var object
     */
    protected $clientRepository;

    /**
     * Instances a new seeder class.
     */
    public function __construct(
        ClientRepositoryInterface $clientRepository
    ) 
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * stored clients in the database.
     *
     * @return \Iterator
     */
    private function registerClients($clients)
    {
        foreach ($clients as $client) {
                $data = [
                    'name' =>  $client['name'],
                    'last_name' => $client['last_name'],
                    'profession' => $client['profession'],
                    'identifier' => $client['identifier']
                ];

                $client_create = $this->clientRepository->create($data);
 
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->registerClients(self::CLIENTS);
    }
}

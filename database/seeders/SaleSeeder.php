<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SaleDetail;
use App\Repositories\Interfaces\SaleRepositoryInterface;
use App\Repositories\Interfaces\ClientRepositoryInterface;
use App\Repositories\Interfaces\PotionRepositoryInterface;

class SaleSeeder extends Seeder
{
    /**
     * @var object
     */
    protected $saleRepository;

    /**
     * @var object
     */
    protected $clientRepository;

    /**
     * @var object
     */
    protected $potionRepository;


    public function __construct(
        SaleRepositoryInterface $saleRepository,
        ClientRepositoryInterface $clientRepository,
        PotionRepositoryInterface $potionRepository
    )
    {
        $this->saleRepository = $saleRepository;
        $this->clientRepository = $clientRepository;
        $this->potionRepository = $potionRepository;
    }

   

    public function readCSV($csvFile, $array)
    {
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 0, $array['delimiter']);
        }
        fclose($file_handle);
        return $line_of_text;
    }

    /**
     * @return void
     */
    protected function createSales()
    {
        $csvFileName = "ventas.csv";
        $csvFile = public_path('csv/' . $csvFileName);

        $sales = $this->readCSV($csvFile,array('delimiter' => ','));

        $sales_register = []; 

        foreach($sales as $sale)
        {  
            if (!is_bool($sale[0])) {
                    $register = false; 

                    for ($i=0; $i < count($sales_register) ; $i++) { 
                            if ($sales_register[$i]['client'] == $sale[0] &&  $sales_register[$i]['date'] == $sale[3]) {
                                $sale_register_id = $sales_register[$i]['sale']; 
                                $register = true ; 
                            }
                    }
                            
                    if ($register != true) {
                        $client = $this->clientRepository->findByIdentifier($sale[0]);

                        $data = [
                            'client_id' =>  $client->id,
                            'date_sale' => $sale[3],
                            'total_amount' => 0
                        ];
            
                    
            
                        $sale_save = $this->saleRepository->create($data);

                        if ($client->id && $sale_save->date_sale && $sale_save->id) {
                            //register sale

                            $register = [];

                            $register =  [
                                'client'    => $sale[0],
                                'date'    => $sale_save->date_sale,
                                'sale' => $sale_save->id
                            ];
            
                            array_push($sales_register, $register);
                        }
                       
                    

                    }else{

                        //find sale
                        $sale_save = $this->saleRepository->find($sale_register_id);
                    }

                

                    $potion = $this->potionRepository->findByIdentifier($sale[1]);

                    $sale_detail = NEW SaleDetail;
                    $sale_detail->potion_id = $potion->id ; 
                    $sale_detail->sale_id = $sale_save->id ;
                    $sale_detail->quantity = $sale[2]; 
                    $sale_detail->amount = $potion->price;
                    $sale_detail->save(); 
                
                    $sale_save->total_amount = $sale_save->total_amount + ($potion->price * $sale[2]);
                    $sale_save->update();
                
            }
        }
    }

  

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createSales();
    }
}

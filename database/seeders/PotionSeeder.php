<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PotionIngredient;
use App\Repositories\Interfaces\PotionRepositoryInterface;
use App\Repositories\Interfaces\IngredientRepositoryInterface;

class PotionSeeder extends Seeder
{
    const POTIONS = [
        [
            'name'    => 'Poción de amor',
            'description'    => 'Endless Love',
            'amount_ingredients' => 4,
            'identifier'          => 'FORM001',
            'ingredients'    => [
                                    [
                                        'identifier' => 'sandals',
                                        'quantity'     => 0.2
                                    ],
                                    [
                                        'identifier' => 'salt',
                                        'quantity'     => 0.1
                                    ],
                                    [
                                        'identifier' => 'wine',
                                        'quantity'     => 0.4
                                    ],
                                    [
                                        'identifier' => 'magic',
                                        'quantity'     => 0.3
                                    ],
                                ]

        ],
        [
            'name'    => 'Poción alisadora',
            'description'    => 'smooth forever',
            'amount_ingredients' => 4,
            'identifier'          => 'FORM002',
            'ingredients'    => [
                                    [
                                        'identifier' => 'ashes',
                                        'quantity'     => 0.3
                                    ],
                                    [
                                        'identifier' => 'aloe',
                                        'quantity'     => 0.3
                                    ],
                                    [
                                        'identifier' => 'kitty',
                                        'quantity'     => 0.1
                                    ],
                                    [
                                        'identifier' => 'jugo',
                                        'quantity'     => 0.3
                                    ],
                                ]

        ],
        [
            'name'    => 'Poción multijugos',
            'description'    => 'juicy',
            'amount_ingredients' => 6,
            'identifier'          => 'FORM003',
            'ingredients'    => [
                                    [
                                        'identifier' => 'leech',
                                        'quantity'     => 0.2
                                    ],
                                    [
                                        'identifier' => 'bicornio',
                                        'quantity'     => 0.1
                                    ],
                                    [
                                        'identifier' => 'kitty',
                                        'quantity'     => 0.3
                                    ],
                                    [
                                        'identifier' => 'magic',
                                        'quantity'     => 0.2
                                    ],
                                    [
                                        'identifier' => 'salt',
                                        'quantity'     => 0.1
                                    ],
                                    [
                                        'identifier' => 'ashes',
                                        'quantity'     => 0.1
                                    ],
                                ]

        ],
    ];
  
    /**
     * @var object
     */
    protected $ingredientRepository;

    /**
     * @var object
     */
    protected $potionRepository;

    /**
     * Instances a new seeder class.
     */
    public function __construct(
        IngredientRepositoryInterface $ingredientRepository,
        PotionRepositoryInterface $potionRepository
    ) 
    {
        $this->ingredientRepository = $ingredientRepository;
        $this->potionRepository = $potionRepository;
    }

    /**
     * stored clients in the database.
     *
     * @return \Iterator
     */
    private function registerPotions($potions)
    {
        $ingredients_array = [];

        foreach ($potions as $potion) {

                
                $data = [
                    'name' =>  $potion['name'],
                    'description' => $potion['description'],
                    'amount_ingredients' => $potion['amount_ingredients'],
                    'identifier' => $potion['identifier']
                ];

                $potion_create = $this->potionRepository->create($data);

                foreach ($potion['ingredients'] as $ingredient) {

                    $find_ingredient = $this->ingredientRepository->findByIdentifier($ingredient['identifier']);

                    $potion_ingredient = NEW PotionIngredient;
                    $potion_ingredient->potion_id = $potion_create->id;
                    $potion_ingredient->ingredient_id = $find_ingredient->id;
                    $potion_ingredient->quantity = $ingredient['quantity'];
                    $potion_ingredient->save(); 
                }
                
 
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->registerPotions(self::POTIONS);
    }
}

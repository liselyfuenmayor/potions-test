<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePotionIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('potion_ingredients', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('potion_id');
            $table->unsignedBigInteger('ingredient_id');
            $table->double('quantity');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('potion_id')
                ->references('id')
                ->on('potions');

            $table->foreign('ingredient_id')
                ->references('id')
                ->on('ingredients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('potion_ingredients');
    }
}

## About Potions Free

Potions Free t is an api to manage an imaginary shop for the sale of magic posions for witches


- [Simple and elegan desing].
- [Easy  to run].


## HOW to run 

- **clone the git repository**
- **execute composer install **
- **create a database with the name "potions_free" **
- **execute " php artisan migrate:refresh --seed " **
- **execute " php artisan serv" ]**
- **use any database user with the password "password" **
- **send the token with each request**
- **Enjoy**

## List Endpoints

- POST [Login] (host/api/auth/login ).
- POST [Logout] (host/api/auth/logout).
- GET [List Users] (host/api/users).
- GET [List Clients] (host/api/clients).
- POST [Store Client] (host/api/clients).
- GET [Show Client] (host/api/clients/{client_id}).
- PUT [Update Client] (host/api/clients/{client_id}).
- DELETE [Delete Client] (host/api/clients/{client_id}).
- GET [Sales Report] (host/api/sales).
- GET [Ingredients Report] (host/api/ingredients).

For more information use the insomnia.json document in the folder "documentation"


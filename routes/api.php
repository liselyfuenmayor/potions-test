<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController; 
use App\Http\Controllers\UserController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ReportController;


Route::middleware('api')->group(function () {
    Route::prefix('auth')->group(function () {
        Route::post('login', [AuthController::class, 'login'])->name('login');
        Route::post('logout', [AuthController::class, 'logout'])->name('logout');
    });
});

Route::middleware('auth:api')->group(function () {

    Route::prefix('users')->group(function () {
        Route::get('/', [UserController::class, 'index'])->name('users');
    });

    Route::prefix('clients')->group(function () {
        Route::get('/', [ClientController::class, 'index'])->name('clients');
        Route::get('/{id}', [ClientController::class, 'show'])->name('clients.show');
        Route::post('/', [ClientController::class, 'store'])->name('clients.store');
        Route::put('/{id}', [ClientController::class, 'update'])->name('clients.update');
        Route::delete('/{id}', [ClientController::class, 'destroy'])->name('clients.destroy');
    });

    // reports

    Route::prefix('sales')->group(function () {
        Route::get('/', [ReportController::class, 'getSalesReport'])->name('sales.report');
    });

    Route::prefix('ingredients')->group(function () {
        Route::get('/', [ReportController::class, 'getIngredientsReport'])->name('ingredients.report');
    });
    
});



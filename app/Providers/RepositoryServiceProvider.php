<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $models = [
            'Client',
            'Potion',
            'Ingredient',
            'Sale',
            'SaleDetail',
            'User',
            'PotionIngredient'
        ];

        foreach ($models as $model) {
            $this->app->bind(
                "App\Repositories\\Interfaces\\{$model}RepositoryInterface",
                "App\Repositories\\{$model}Repository"
            );
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}

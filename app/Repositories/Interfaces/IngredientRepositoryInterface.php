<?php

namespace  App\Repositories\Interfaces;

interface IngredientRepositoryInterface
{
    public function getIngredientsReport($parameters);
}
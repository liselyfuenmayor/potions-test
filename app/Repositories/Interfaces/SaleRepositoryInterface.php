<?php

namespace  App\Repositories\Interfaces;

interface SaleRepositoryInterface
{
    public function getSalesReport($parameters);
}
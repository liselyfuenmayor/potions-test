<?php

namespace App\Repositories;

use App\Models\Client;
use App\Repositories\Interfaces\ClientRepositoryInterface;

class ClientRepository extends BaseRepository implements ClientRepositoryInterface
{
    /**
     * @var string
    */
    protected $table = 'clients';

    /**
     * @var object
    */
    protected $model;

    public function __construct(
        Client $model
    )
    {
        $this->model = $model;

        parent::__construct($this->model);
    }



    
}
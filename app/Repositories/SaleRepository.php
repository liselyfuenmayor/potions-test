<?php

namespace App\Repositories;

use App\Models\Sale;
use Illuminate\Support\Facades\DB;
use App\Repositories\Interfaces\SaleRepositoryInterface;

class SaleRepository extends BaseRepository implements SaleRepositoryInterface
{
    /**
     * @var string
    */
    protected $table = 'sales';

    /**
     * @var object
    */
    protected $model;

    public function __construct(
        Sale $model
    )
    {
        $this->model = $model;

        parent::__construct($this->model);
    }

    public function getSalesReport($parameters)
    {
        $client_filter = []; 

        if ($parameters['id'] != null) {

            $client_filter =['sales.client_id' => $parameters['id']]  ;

        }
        $sales = DB::table('sales')
                ->select('potions.name',DB::RAW('sum(sale_details.quantity) as quantity'),'sale_details.amount as price_potion',DB::RAW('sum(sale_details.amount * sale_details.quantity)  as total_potion '))
                ->leftJoin('sale_details', 'sales.id', '=', 'sale_details.sale_id')
                ->leftJoin('potions', 'potions.id', '=', 'sale_details.potion_id')
                ->where($client_filter)
                ->where('sales.date_sale','>=',$parameters['from'])
                ->where('sales.date_sale','<=',$parameters['to'])
                ->groupBy('potions.name','amount');


                return  $sales->get();
       
    }

}
<?php

namespace App\Repositories;

use App\Models\Potion;
use App\Repositories\Interfaces\PotionRepositoryInterface;

class PotionRepository extends BaseRepository implements PotionRepositoryInterface
{
    /**
     * @var string
    */
    protected $table = 'potions';

    /**
     * @var object
    */
    protected $model;

    public function __construct(
        Potion $model
    )
    {
        $this->model = $model;

        parent::__construct($this->model);
    }



    
}
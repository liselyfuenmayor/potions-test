<?php

namespace App\Repositories;

use App\Models\Ingredient;
use Illuminate\Support\Facades\DB;
use App\Repositories\Interfaces\IngredientRepositoryInterface;

class IngredientRepository extends BaseRepository implements IngredientRepositoryInterface
{
    /**
     * @var string
    */
    protected $table = 'ingredients';

    /**
     * @var object
    */
    protected $model;

    public function __construct(
        Ingredient $model
    )
    {
        $this->model = $model;

        parent::__construct($this->model);
    }

public function getIngredientsReport($parameters)
{
    $potion_filter = []; 

    if ($parameters['id'] != null) {

        $potion_filter =['potions.id' => $parameters['id']]  ;

    }

    $ingredients = DB::table('ingredients')

            ->select('ingredients.name',DB::RAW('round(SUM(potion_ingredients.quantity),3) as quantity_used'),'ingredients.price', DB::RAW('round (( round(SUM(potion_ingredients.quantity),3) ) * ingredients.price,2) as total_amount'))
            ->leftJoin('potion_ingredients', 'potion_ingredients.ingredient_id', '=', 'ingredients.id')
            ->leftJoin('potions', 'potions.id', '=', 'potion_ingredients.potion_id')
            ->leftJoin('sale_details', 'potions.id', '=', 'sale_details.potion_id')
            ->leftJoin('sales', 'sales.id', '=', 'sale_details.sale_id')
            ->where($potion_filter)
            ->where('sales.date_sale','>=',$parameters['from'])
            ->where('sales.date_sale','<=',$parameters['to'])
            ->groupBy('ingredients.name','ingredients.price');


            return  $ingredients->get();
}

    
}
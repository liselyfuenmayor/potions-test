<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class BaseRepository
{
    /**
     * @var object
     */
    protected $model;


    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Create an object model.
     *
     * @param array $data
     *
     * @return object the object create
     *
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Update an object model.
     *
     * @param int|array|object $component
     * @return bool the object update
     *
     */
    public function update(object $object)
    {

        return $object->update();

    }

    /**
     * Delete an object model.
     *
     * @param int $id
     * @return bool the object delete
     *
     */
    public function delete($id)
    {
        $object = $this->find($id);

        if($object) {
            return $object->delete();
        }

        return false;
    }


    /**
     * Finds all models in the repository.
     * @return array The models.
     */

    public function findAll()
    {
        return $this->model::all();
    }

    /**
     * Finds an model by its primary key / identifier.
     *
     * @param mixed    $id          The identifier.
     * @return object|null The model instance or NULL if the model can not be found.
     */
    public function find($id)
    {
        $query = $this->model;

        return $query->find($id);
    }

     /**
     * Finds an model by its  identifier code.
     *
     * @param mixed    $identifier          The identifier.
     * @return object|null The model instance or NULL if the model can not be found.
     */
    public function findByIdentifier($identifier)
    {
        $query = $this->model
                ->where('identifier',$identifier);

        return $query->first();
    }

    


}

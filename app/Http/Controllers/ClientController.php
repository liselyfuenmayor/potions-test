<?php

namespace App\Http\Controllers;

use App\Models\Client; 
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Repositories\Interfaces\ClientRepositoryInterface;
use App\Http\Requests\ClientRequest;
use App\Http\Requests\ClientUpdateRequest;

class ClientController extends Controller
{
    /**
     * @var object
     */
    protected $clientRepository;

    /**
     * Instances a new seeder class.
     */
    public function __construct(
        ClientRepositoryInterface $clientRepository
    ) 
    {
        $this->clientRepository = $clientRepository;
    }
    
    /**
     * Display a listing of test types.
     * @return Response
     */
    public function index()
    {
        $clients = $this->clientRepository->findAll(); 

        return $this->sendResponse($clients, 'List of Clients');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(ClientRequest $request)
    {
        try {
            $client = $this->clientRepository->findByIdentifier($request['identifier']);

            if ($client) {
                return $this->sendError('Client already exists');
            }

            $client = $this->clientRepository->create($request->all());

            return $this->sendResponse($client, 'Client stored', Response::HTTP_CREATED);
        } 
        catch (Exception $exception) {
            return $this->sendError('Error by creating Client', $exception->getMessage());
        }
    }

      /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $client = $this->clientRepository->find($id);

            return $this->sendResponse($client, 'Client information');
        } catch (Exception $exception) {
            return $this->sendError('Error by retrieving Client', $exception->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     * @param ClientUpdateRequest $request
     * @param int $id
     * @return Response
     */
    public function update(ClientUpdateRequest $request, $id)
    {
        try {
            $client = $this->clientRepository->find($id);

            $client->name = $request['name'];
            $client->last_name = $request['last_name'];
            $client->profession = $request['profession'];

            $updated = $this->clientRepository->update($client);

            return $this->sendResponse($updated, 'Client data updated');

        } catch (Exception $exception) {
            return $this->sendError('Error by updating Client', $exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
         $return = $this->clientRepository->delete($id) 
                   ?$this->sendResponse(NULL, 'Client deleted', Response::HTTP_ACCEPTED) 
                   :$this->sendError('Client Not Existing');
      
         return $return; 
        } catch (Exception $exception) {
            return $this->sendError('Error by deleting Client', $exception->getMessage());
        }
    }




}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Repositories\Interfaces\SaleRepositoryInterface;
use App\Repositories\Interfaces\IngredientRepositoryInterface;

class ReportController extends Controller
{
    /**
     * @var object
     */
    protected $saleRepository;

    /**
     * @var object
     */
    protected $ingredientRepository;

    /**
     * Instances a new seeder class.
     */
    public function __construct(
        SaleRepositoryInterface $saleRepository,
        IngredientRepositoryInterface $ingredientRepository
    ) 
    {
        $this->saleRepository = $saleRepository;
        $this->ingredientRepository = $ingredientRepository;
    }
    
    /**
     * Display a report number 1.
     * @return Response
     */
    public function getSalesReport(Request $request)
    {
        $sales = $this->saleRepository->getSalesReport($request->all()); 

        return $this->sendResponse($sales, 'Sales Report');
    }

    /**
     * Display a report number 2.
     * @return Response
     */
    public function getIngredientsReport(Request $request)
    {
        $ingredients = $this->ingredientRepository->getIngredientsReport($request->all()); 

        return $this->sendResponse($ingredients, 'Ingredients Report');
    }

}

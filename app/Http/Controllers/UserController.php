<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User; 
class UserController extends Controller
{
    
    /**
     * Display a listing of test types.
     * @return Response
     */
    public function index()
    {
        $users = User::all();

        return $this->sendResponse($users, 'List of Users');
    }

}

<?php

namespace App\Models;

use App\Models\Ingredient;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Potion extends Model
{
    use SoftDeletes;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'potions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'amount_ingredients',
        'identifier'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be cast to datetime types.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * 
     *
     * @var array
     */
    protected $appends = [
        'price'
    ];

    /**
     * Returns the ingredients  object related to the  Potion
     * 
     * @return Array
     */
    public function ingredients () 
    {
        return $this->belongsToMany(
            Ingredient::class, 'potion_ingredients'
        )->withTimestamps()->withPivot('quantity');
    }

    /**
     * Get the  calculated attribute
     */
    public function getPriceAttribute()
    {
        $value_potion = 0; 
        foreach ($this->ingredients as $ingredient) {
            $value = $ingredient->price * $ingredient->pivot->quantity ; 

            $value_potion = $value_potion + $value ; 
        }
        return $value_potion;
    }


}
